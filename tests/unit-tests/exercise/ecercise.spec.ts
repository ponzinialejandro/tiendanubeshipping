import 'jest';
import {describe, it, expect} from '@jest/globals';
import {ShippingCostSorter} from "../../../src/excercise/ShippingCostSorter";
import {ShippingCost} from "../../../src/excercise/ShippingCost";


describe("Example", () => {


    describe("sorting shippings ", () => {


        const tt = [
            {
                name: "Empty shipping",
                getResponse: [],
                expectedOutput: []
            },
            {
                name: "same shipping cost, order them by lower estimation days",
                getResponse: [{"name": "Option 1", "type": "Delivery", "cost": 10, "estimated_days": 5},
                    {"name": "Option 2", "type": "Custom", "cost": 10, "estimated_days": 7},
                    {"name": "Option 3", "type": "Pickup", "cost": 10, "estimated_days": 4}],
                expectedOutput: [{"name": "Option 3", "type": "Pickup", "cost": 10, "estimated_days": 4},
                    {"name": "Option 1", "type": "Delivery", "cost": 10, "estimated_days": 5},
                    {"name": "Option 2", "type": "Custom", "cost": 10, "estimated_days": 7}]
            },
            {
                name: "same shipping estimation days, order them by lower estimation delivery cost",
                getResponse: [{"name": "Option 1", "type": "Delivery", "cost": 7, "estimated_days": 3},
                    {"name": "Option 2", "type": "Custom", "cost": 1, "estimated_days": 3},
                    {"name": "Option 3", "type": "Pickup", "cost": 9, "estimated_days": 3}],
                expectedOutput: [{"name": "Option 2", "type": "Custom", "cost": 1, "estimated_days": 3},
                    {"name": "Option 1", "type": "Delivery", "cost": 7, "estimated_days": 3},
                    {"name": "Option 3", "type": "Pickup", "cost": 9, "estimated_days": 3}]
            },
            {
                name: "different delivery cost and estimation dates, sort them first by cost then by estimation days",
                getResponse: [{"name": "Option 4", "type": "Delivery", "cost": 10, "estimated_days": 3},
                    {"name": "Option 1", "type": "Delivery", "cost": 10, "estimated_days": 5},
                    {"name": "Option 2", "type": "Custom", "cost": 5, "estimated_days": 4},
                    {"name": "Option 3", "type": "Pickup", "cost": 7, "estimated_days": 1}],
                expectedOutput: [{"name": "Option 2", "type": "Custom", "cost": 5, "estimated_days": 4},
                    {"name": "Option 3", "type": "Pickup", "cost": 7, "estimated_days": 1},
                    {"name": "Option 4", "type": "Delivery", "cost": 10, "estimated_days": 3},
                    {"name": "Option 1", "type": "Delivery", "cost": 10, "estimated_days": 5}]
            }
            ]

        tt.forEach(tc => {
            it(`Case : ${tc.name}`, () => {
                const sortedArray = ShippingCostSorter.sort(ShippingCost.FromJson(tc.getResponse))
                expect(sortedArray).toStrictEqual(ShippingCost.FromJson(tc.expectedOutput))
            })
        })
    })
})
