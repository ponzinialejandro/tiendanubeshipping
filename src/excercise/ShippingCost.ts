export class ShippingCost {
    constructor(public name: string, public type: string, public cost: number, public estimated_days: number) {
    }

    static FromJson(mockedValue: Array<{
        name: string,

        type: string,
        cost: number,
        estimated_days: number
    }>): Array<ShippingCost> {

        return mockedValue.map(mv => new ShippingCost(mv.name, mv.type, mv.cost, mv.estimated_days));
    }
}