import {ShippingCostSorter} from "./ShippingCostSorter";
import {ShippingCostResolver} from "./ShipingConstResolver";
import {ShippingCost} from "./ShippingCost";

export class ShippingCostUseCase{
    constructor(private shippingCostResolver :ShippingCostResolver ) {
    }

    public async Execute( zipCode : string ): Promise<Array<ShippingCost>>{
        if(!zipCode){
            throw new Error("Invalid zip code")
        }
        let costs: Array<ShippingCost> = await this.shippingCostResolver.resolverShippingCostForZipCode(zipCode)
        ShippingCostSorter.sort(costs);
        return costs
    }
}