import axios from "axios";
import {ShippingCostSorter} from "./ShippingCostSorter";
import {ShippingCost} from "./ShippingCost";

export interface ShippingCostResolver{
    resolverShippingCostForZipCode(zipCode :string): Promise<Array<ShippingCost>>
}

export class DummyShippingCostResolver implements ShippingCostResolver{
    private static mockedValue = [{"name": "Option 2", "type": "Custom", "cost": 5, "estimated_days": 4},
        {"name": "Option 3", "type": "Pickup", "cost": 7, "estimated_days": 1},
        {"name": "Option 4", "type": "Delivery", "cost": 10, "estimated_days": 3},
        {"name": "Option 1", "type": "Delivery", "cost": 10, "estimated_days": 5}]
    resolverShippingCostForZipCode(zipCode: string): Promise<Array<ShippingCost>> {
        return Promise.resolve(ShippingCost.FromJson(DummyShippingCostResolver.mockedValue))
    }
}
export class EnvioNubesShippingCostResolver implements ShippingCostResolver{
    private static SERVICE_URL = ""

    resolverShippingCostForZipCode(zipCode: string): Promise<Array<ShippingCost>> {
        return axios.get(EnvioNubesShippingCostResolver.SERVICE_URL)
        .then(response => {
           //TODO parse objet into my interface type
            return []
        })
    }
}


