import {ShippingCostUseCase} from "./ShippingCostUseCase";
import {DummyShippingCostResolver, EnvioNubesShippingCostResolver, ShippingCostResolver} from "./ShipingConstResolver";


export class UseCaseFactory{

    constructor(private provider: string ) {
    }

    public buildGetShippingCostUseCase() : ShippingCostUseCase{
        return new ShippingCostUseCase(this.getShippingCostResolver())
    }

    private getShippingCostResolver(): ShippingCostResolver{
        switch (this.provider) {
            case  "local" :
                return new DummyShippingCostResolver()
            case "envioNubes":
                return new EnvioNubesShippingCostResolver()
            default :
                throw new Error(`provider ${this.provider} not supported`)
        }
    }
}