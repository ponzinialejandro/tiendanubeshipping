import {Express} from 'express'
var express = require('express')
const app:Express = express();

import {UseCaseFactory} from "./useCaseFactory";

const port = 3000

const provider  = "local" //fetchIt from process.env
const useCaseFactory = new UseCaseFactory(provider)

app.get('/shipping-cost', (req, res) => {
    useCaseFactory.buildGetShippingCostUseCase()
        .Execute('123ks')
        .catch(err =>{
            res.status(500)
        })
        .then( response =>{
            res.send(response)
    })
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})