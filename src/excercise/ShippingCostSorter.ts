import {ShippingCost} from "./ShippingCost";

export class ShippingCostSorter {
    public static sort(input: Array<ShippingCost>): Array<ShippingCost> {

        input.sort((a, b) => {
            if (a.cost === b.cost) {
                if (a.estimated_days <= b.estimated_days) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (a.cost < b.cost) {
                return -1;
            } else {
                return 1;
            }
        });
        return input;
    }
}